﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pooling {
    public class PoolObjectBehaviour : MonoBehaviour
    {
        IPoolObject _poolObject;

        public void SetPoolObject(IPoolObject poolObject) {
            if (_poolObject != null) {
                _poolObject.AwakeFromPoolEvent -= OnAwakeFromPool;
                _poolObject.ReturnToPoolEvent -= OnReturnToPool;
                _poolObject = null;
            }

            _poolObject = poolObject;
            if (_poolObject != null) {
                _poolObject.AwakeFromPoolEvent += OnAwakeFromPool;
                _poolObject.ReturnToPoolEvent += OnReturnToPool;
            }
        }

        protected virtual void OnAwakeFromPool() { }

        protected virtual void OnReturnToPool() { }
    }
}


